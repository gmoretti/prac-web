<?php
  require_once('header.html');
  $conocenos = <<<END
  			<h2>Conócenos</h2>
  			<div class="float">
  				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d11927.802128074165!2d-87.619620984485!3d41.63520068003089!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x880e215d7de302f5%3A0x3ec767c10f432c63!2sAdams+St%2C+Dolton%2C+IL+60419%2C+EE.+UU.!5e0!3m2!1ses!2ses!4v1444645314360" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
  			</div>
  			<div class="float" id="contacto">
  				<ul>
  					<li> <b>Direccion:</b> 3056 Adams St<br /> Dolton, IL 60419 </li>
  					<li> <b>Telefono:</b> 202-555-0182  </li>
  					<li> <b>e-mail:</b> contacto@tempo.com </li>
  				</ul>
  			</div>
			<div class="clear" />
		</article>
END;
  echo $conocenos;
  require_once('footer.html');
?>
