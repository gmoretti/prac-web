<?php
  require_once('header.html');

  /*Accedemos al controlador para conseguirlas categorías
  y montar el codigo HTML que luego insertaremos con una variable.*/
  $arr = ctlr_getCategories();
  $categorias = "";
  foreach ($arr as $categoryId => $categoryName) {
    $categorias .= "<option value=".$categoryId."> ".$categoryName."</option>";
  }

  $catalogo = <<<END

  			<h2>Catálogo</h2>
  			<div id="bloqueSelectorCategorias">
  				<p class="float">Selecciona la categoria: </p>
  				<select id="catSel" onchange="ajaxCatalogoReload($('#catSel').val());" name="catSel">
              //Esta variable contiene el html montado.
              $categorias
          </select>
  			</div>

  			<div id="contenedorProductos">
  			</div>
        <script>ajaxCatalogoReload($('#catSel').val());</script>
		</article>
END;
  echo $catalogo;
  require_once('footer.html');
?>
