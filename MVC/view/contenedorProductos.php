<?php
  /*Este fichero monta el html necesario para mostrar un grupo de productos, agrupados por su categoria,
  la llamada proviene de una llamada ajax desde el cartalogo, donde como parametro GET conseguimos el ID que el cliente selecciona.
  Una vez conseguido el id, con un controlador, accedemos al modelo para los datos.*/

  require_once('../controller/ajax_controller.php');
  $categoryId = $_GET['id'];

  $productos = ctlr_getProductosByCategoryId($categoryId);

  $productosHTML = "";
  foreach ($productos as $productId => $product) {
    $nombre = $product['nombre'];
    $precio = $product['precio'];

    $productosHTML .=<<<END
    <div class="producto">
      <!-- Probablemente lo cambiemos porque es mas adecuado ponerlo en el css-->
      <img src="../controller/product_image_controller.php?id=$productId" />
      <ul type="none">
        <li> $nombre </li>
        <li> $precio € </li>
      </ul>
    </div>
END;
  }
  $productosHTML .= "<div class='clear' />";
  echo $productosHTML;

?>
