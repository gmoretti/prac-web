<?php
  require_once('../model/catalogo_model.php');


  function ctlr_getCategories(){
    $categoriesArray = getCategories();
    return $categoriesArray;
  }

  function ctlr_getProductosByCategoryId($categoryId){
    $productsArray = getProductosByCategoryId($categoryId);
    return $productsArray;
  }


  require_once('../view/catalogo_view.php');
 ?>
