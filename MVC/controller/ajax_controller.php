<?php
  /*Este controlador es necesario para ejecutar
  las llamadas ajax externas dinamicas que no provienen
  de un controlador especifico ypor tanto al no pertenecer
   a una página no es necesario cargar footer y header*/

  require_once('../model/catalogo_model.php');

  //Esta funcion deberia ser igual a la incluida en el catalogo_controller.php
  function ctlr_getCategories(){
    $categoriesArray = getCategories();
    return $categoriesArray;
  }

  //Esta funcion deberia ser igual a la incluida en el catalogo_controller.php
  function ctlr_getProductosByCategoryId($categoryId){
    $productsArray = getProductosByCategoryId($categoryId);
    return $productsArray;
  }
 ?>
