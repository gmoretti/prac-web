<?php
  /*Este sript accede al modelo de catalogo para
  conseguir los datos de la columna de imagen dado un ID de producto,
  Estos datos se devuelven el a peticion GET con tipo de documento imagen/jpg*/
  require_once('../model/catalogo_model.php');
  $productId = $_GET['id'];

  $productImg = getImageById($productId);
  header("Content-type: image/jpg");
  echo $productImg;
?>
