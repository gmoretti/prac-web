<?php
  require_once('connection.php');

  /*Devuelve todas las categorías disponibles en un array*/
  function getCategories(){
    global $conn;
    $query = "SELECT * FROM categoria";
    $res = mysqli_query($conn,$query);
    $arr = array();
    if ($res) {
      while($row = $res->fetch_array()) {
        $arr[$row[0]] = $row[1];
      }
    }
    return $arr;
  }

  /*Devuelve todos los produtos*/
  function getProductos(){
    global $conn;
    $query = "SELECT * FROM producto";
    $res = mysqli_query($conn,$query);
    $productos = array();
    if ($res) {
      while($row = $res->fetch_array()) {
        $producto = array();
        $producto['nombre'] = $row[1];
        $producto['precio'] = $row[2];
        $producto['descripcion'] = $row[3];
        #$producto['imagen'] = $row[4];
        $producto['stock'] = $row[5];
        $producto['idCat'] = $row[6];
        $productos[$row[0]] = $producto;
      }
    }
    return $productos;
  }

  /*Genera un array de arrays de productos con sus datos, dado un id de categoria*/
  function getProductosByCategoryId($categoryId){
    global $conn;
    $query = "SELECT * FROM producto WHERE id_categoria=".$categoryId;
    $res = mysqli_query($conn,$query);
    $productos = array();
    if ($res) {
      while($row = $res->fetch_array()) {
        $producto = array();
        $producto['nombre'] = $row[1];
        $producto['precio'] = $row[2];
        $producto['descripcion'] = $row[3];
        #$producto['imagen'] = $row[4];
        $producto['stock'] = $row[5];
        $producto['idCat'] = $row[6];
        $productos[$row[0]] = $producto;
      }
    }
    return $productos;
  }

  /*Seleciona los datos en crudo de la imagen BLOB guardada
  en la base de datos yretorna en uan variable con su contenido;
  Asumimos que nunca sera más de una*/
  function getImageById($id){
    global $conn;
    $query = "SELECT producto.imagen FROM producto WHERE id_producto=".$id;
    $res = mysqli_query($conn,$query);
    if ($res) {
      $row = $res->fetch_array();
    }
    $image = $row[0];
    return $image;
  }

  //TODO: Gestionar el cierre de coenxiones.
  //mysql_close($conn);
?>
