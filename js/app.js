//Ajax para cargar el formulario de registro al clickar en Registrarse.
function ajaxLoadRegisterForm(){
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 4 && xhttp.status == 200) {
      document.getElementById("mainContainer").innerHTML = xhttp.responseText;
    }
  }
  xhttp.open("GET", "../../MVC/view/formularioRegistro.html", true);
  xhttp.send();
}

/*Ajax para la recarga de productos dependiendo de la categoría seleccionada
en el catalogo*/
function ajaxCatalogoReload(idCategoria){
  //console.log(idCategoria);
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 4 && xhttp.status == 200) {
      document.getElementById("contenedorProductos").innerHTML = xhttp.responseText;
    }
  }
  xhttp.open("GET", "../view/contenedorProductos.php?id="+idCategoria, true);
  xhttp.send();
}
