/*Funcion que limpia el texto del div que contiene el error en cada elemento,
para ser usado antes de la vaildacion */
function cleanFormRegistro(){
  $('#err_nombre').text("");
  $('#err_apellido').text("");
  $('#err_usuario').text("");
  $('#err_contrasena').text("");
  $('#err_contrasenaConf').text("");
  $('#err_direccion').text("");
  $('#err_email').text("");
  $('emailConf').text("");
  $('#err_numeroCuenta').text("");
  return 0;
}

/*Validaciíon del Formulario de registro*/
function validarFormulario() {

  /*Primero Limpiamos el Formulario de la validación anterior*/
  cleanFormRegistro();

  /*TODO:Este array servira para comprobar que todas las validaciones son correctas, antes de enviar el formulario; Falta parte de la implementación*/
  form_check_array = {"nombre":true,"apellido":true};

	//Nombre
  var patt = /^[A-Za-z ]*[A-Za-z][A-Za-z ]*$/i;
	var x = document.forms["formulario"]["nombre"];
	if (x.value == null || x.value == "") {
		$('#err_nombre').text("El campo no puede estar vacio.");
    form_check_array["nombre"] = false;
  }else if(!patt.test(x.value)){
    $('#err_nombre').text("El nombre no puede contener carácteres especiales.");
    form_check_array["nombre"] = false;
  }

  //Apellido
  var patt = /^[A-Za-z ]*[A-Za-z][A-Za-z ]*$/i;
	var x = document.forms["formulario"]["apellido"];
	if (x.value == null || x.value == "") {
		$('#err_apellido').text("El campo no puede estar vacio.");
	}else if(!patt.test(x.value)){
    $('#err_apellido').text("El apellido no puede contener carácteres especiales.");
  }

	//Usuario
  user_check = true;
  patt = /^[A-Za-z0-9_]*[A-Za-z0-9][A-Za-z0-9_]*$/i;
	var x = document.forms["formulario"]["usuario"];
	if (x.value == null || x.value == "") {
		$('#err_usuario').text("El campo no puede estar vacio.");
    user_check = false;
	}else if(!patt.test(x.value)){
    $('#err_usuario').text("El usuario no puede contener letras, numeros y '_'.");
    user_check = false;
  }


	//Password
  patt_min = /^.[^ ]{6,}$/i;
  patt_max = /^.[^ ]{0,20}$/i;
	var x = document.forms["formulario"]["contrasena"];
	if (x.value == null || x.value == "") {
		$('#err_contrasena').text("El campo no puede estar vacio.");
	}else if(!patt_min.test(x.value)){
    $('#err_contrasena').text("La contraseña debe tener un minimo de 6 carácteres y no puede contener espacios.");
  }else if(!patt_max.test(x.value)){
    $('#err_contrasena').text("La contraseña debe tener un máximo de 20 carácteres y no puede contener espacios.");
  }

	//PasswordConf
	var y = document.forms["formulario"]["contrasenaConf"];
	if (y.value == null || y.value == "") {
		$('#err_contrasenaConf').text("El campo no puede estar vacio.");
	}
	else if(x.value != y.value){
		$('#err_contrasenaConf').text("Las contraseñas no coinciden.");
	}

	//Direccion
  var patt = /^[A-Za-z0-9 \/ºª]*[A-Za-z][A-Za-z0-9 \/ºª]*$/i;
	var x = document.forms["formulario"]["direccion"];
	if (x.value == null || x.value == "") {
		$('#err_direccion').text("El campo no puede estar vacio.");
	}else if(!patt.test(x.value)){
    $('#err_direccion').text("La direccion tiene carácteres que no se permiten.");
  }

	//Email
	var patt = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;

	var x = document.forms["formulario"]["email"];

	if (x.value == null || x.value == "") {
		$('#err_email').text("El campo no puede estar vacio.");
	}
	else if(!patt.test(x.value)){
		$('#err_email').text("El email no esta bien formado.");
	}


	//EmailConf
	var y = document.forms["formulario"]["emailConf"];
	if (y.value == null || y.value == "") {
			$('emailConf').text("El campo no puede estar vacio.");
	}
	else if(x.value != y.value){
			$('emailConf').text("Los emails no coinciden.");
	}

	//NumeroCuenta
  var patt = /^[A-Za-z0-9]*[A-Za-z][A-Za-z0-9]*$/i;
	var x = document.forms["formulario"]["numeroCuenta"];
  if(!patt.test(x.value)){
    $('#err_numeroCuenta').text("El numero de cuenta no es valido.");
  }
	return 0;
}
